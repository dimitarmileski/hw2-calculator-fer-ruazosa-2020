package com.example.calculator.model

import com.example.calculator.utils.ExpressionEvaluator

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {
    var result: Double = 0.0
    private set
    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset(){
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String){
       try {
           val numberAsString = number.toDouble()
       }catch (exc: NumberFormatException){
           throw Exception("Number is not in correct format")
       }

        if (expression.count() % 2 != 0){
            throw Exception("Incorrect number order exception")
        }

        expression.add(number)
    }

    fun addOperator(operator: String){
        if(expression.count() % 2 != 1){
            throw Exception("Incorrect operator order")
        }
        when(operator){
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> throw Exception("Incorrect operator")
        }
    }

    fun evaluate(){
        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        var exp: String = ""
        for (i in 0..expression.size-1){
            exp += expression.get(i)
        }

        var expressionEvaluator =
            ExpressionEvaluator(
                exp,
                ExpressionEvaluator.EXPRESSIONTYPE.Infix
            )
        result = expressionEvaluator.GetValue()
    }

}

