package com.example.calculator.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.calculator.R
import com.example.calculator.databinding.ActivityMainBinding
import com.example.calculator.model.Calculator
import com.example.calculator.viewmodels.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private var resultView: TextView? = null
    private var buttonZero: Button? = null
    private var buttonOne: Button? = null
    private var buttonTwo: Button? = null
    private var buttonThree: Button? = null
    private var buttonFour: Button? = null
    private var buttonFive: Button? = null
    private var buttonSix: Button? = null
    private var buttonSeven: Button? = null
    private var buttonEight: Button? = null
    private var buttonNine: Button? = null
    private var buttonComma: Button? = null
    private var buttonReset: Button? = null
    private var buttonPlus: Button? = null
    private var buttonMinus: Button? = null
    private var buttonEvaluate: Button? = null
    private var buttonMultiply: Button? = null
    private var buttonDivide: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val vm = ViewModelProviders.of(this)[MainActivityViewModel::class.java]

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewmodel = vm
        binding.lifecycleOwner = this


        buttonReset = findViewById(R.id.button_reset)
        resultView = findViewById(R.id.result_view)
        buttonZero = findViewById(R.id.button_zero)
        buttonOne = findViewById(R.id.button_one)
        buttonTwo = findViewById(R.id.button_two)
        buttonThree = findViewById(R.id.button_three)
        buttonFour = findViewById(R.id.button_four)
        buttonFive = findViewById(R.id.button_five)
        buttonSix = findViewById(R.id.button_six)
        buttonSeven = findViewById(R.id.button_seven)
        buttonEight = findViewById(R.id.button_eight)
        buttonNine = findViewById(R.id.button_nine)
        buttonComma = findViewById(R.id.button_comma)
        buttonPlus = findViewById(R.id.button_plus)
        buttonMinus = findViewById(R.id.button_minus)
        buttonEvaluate = findViewById(R.id.button_evaluate)
        buttonMultiply = findViewById(R.id.button_multiply)
        buttonDivide = findViewById(R.id.button_divide)



        buttonReset?.setOnClickListener(vm.reset)

        buttonComma?.setOnClickListener(vm.buttonCommaClicked)

        buttonZero?.setOnClickListener(vm.numericButtonClicked)
        buttonOne?.setOnClickListener(vm.numericButtonClicked)
        buttonTwo?.setOnClickListener(vm.numericButtonClicked)
        buttonThree?.setOnClickListener(vm.numericButtonClicked)
        buttonFour?.setOnClickListener(vm.numericButtonClicked)
        buttonFive?.setOnClickListener(vm.numericButtonClicked)
        buttonSix?.setOnClickListener(vm.numericButtonClicked)
        buttonSeven?.setOnClickListener(vm.numericButtonClicked)
        buttonEight?.setOnClickListener(vm.numericButtonClicked)
        buttonNine?.setOnClickListener(vm.numericButtonClicked)

        buttonPlus?.setOnClickListener(vm.operatorButtonClicked)
        buttonMinus?.setOnClickListener(vm.operatorButtonClicked)
        buttonMultiply?.setOnClickListener(vm.operatorButtonClicked)
        buttonDivide?.setOnClickListener(vm.operatorButtonClicked)


    }
}
