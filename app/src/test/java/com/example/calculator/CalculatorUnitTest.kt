package com.example.calculator

import com.example.calculator.model.Calculator
import org.junit.Assert
import org.junit.Test
import java.lang.Exception

class CalculatorUnitTest {
    @Test
    fun test_Reset() {
        Calculator.reset()
        Assert.assertEquals(Calculator.result, 0.0, 0.0)
        Assert.assertEquals(Calculator.expression.count(), 0)
    }

    @Test
    fun test_addNumber(){
        Calculator.reset()
        Calculator.addNumber("100")
        Assert.assertEquals(Calculator.expression.count(), 1)

        try {
            Calculator.addNumber("200")
        }catch (exc: Exception){
            Assert.assertEquals(exc.message, "Incorrect number order exception")
        }

        try {
            Calculator.addNumber("test")
        }catch (exc: Exception){
            Assert.assertEquals(exc.message, "Number is not in correct format")
        }

    }

    @Test
    fun test_addOperator(){
        Calculator.reset()
        Calculator.addNumber("100")
        Calculator.addOperator("+")
        Calculator.addNumber("200")
        try {
            Calculator.addOperator("/")
        }catch (exc: Exception){
            Assert.assertEquals(exc.message, "Incorrect operator")
        }

        Calculator.reset()
        try {
            Calculator.addOperator("+")
        }catch (exc: Exception){
            Assert.assertEquals(exc.message, "Incorrect operator order")
        }

    }
    @Test
    fun test_evaluate(){
        Calculator.addNumber("2")
        Calculator.addOperator("+")
        Calculator.addNumber("3")
        Calculator.addOperator("*")
        Calculator.addNumber("1")
        Calculator.addOperator("-")
        Calculator.addNumber("9")
        Calculator.addOperator("+")
        Calculator.addNumber("8")
        Calculator.addOperator("/")
        Calculator.addNumber("2")

        Calculator.evaluate()
        Assert.assertEquals(Calculator.result,  0.0, 0.0)
    }

}