package com.example.calculator.viewmodels

import android.os.Handler
import android.view.View
import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.calculator.model.Calculator

class MainActivityViewModel: ViewModel()  {

    private val handler = Handler()

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> get() = _message

    var inOperatorMode: Boolean = false
    var inResultMode: Boolean = false

    val numericButtonClicked = {view: View ->

        val buttonValue = (view as Button).text.toString()
        if (!_message.value.equals("0") && inOperatorMode == false
            && inResultMode == false) {
            _message.value = ""
            _message.value = _message.value + buttonValue
        }
        else {
            inOperatorMode = false
            inResultMode = false
            _message.value = buttonValue
        }

    }

    val reset = { view: View->
        Calculator.reset()
        _message.value = "0"
    }

    val buttonCommaClicked = { view: View ->
        if (!_message.value.toString().contains(char = '.')) {
            _message.value = _message.value.toString() + ".";
        }
    }

    val operatorButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            Calculator.addNumber(_message.value.toString())
            Calculator.addOperator(buttonValue)
            inOperatorMode = true
        }

    fun onLoginClicked() {
        handler.postDelayed({

            if (inOperatorMode) {
                Calculator.addNumber("0")
                inOperatorMode = false
            }
            else {
                Calculator.addNumber(_message.value.toString())
            }
            Calculator.evaluate()
           _message.value = Calculator.result.toString()
            inResultMode = true


            val result = Calculator.result.toString()
            _message.value = result

            Calculator.reset()
        }, 0)
    }





}